package Services

import (
	"encoding/json"
	"io/ioutil"
)

type Settings struct {
	ConnectionString string `json:"ConnectionString"`
	SecretKey        string `json:"SecretKey"`
}

var loadedSettings Settings
var loaded = false

func GetConnectionString() string {
	if !loaded {
		loadSettings()
	}
	return loadedSettings.ConnectionString
}

func GetSecretKey() []byte {
	if !loaded {
		loadSettings()
	}
	return []byte(loadedSettings.SecretKey)
}

func loadSettings() {
	file, err := ioutil.ReadFile("./settings.json")

	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(file), &loadedSettings)

	if err != nil {
		return
	}

	loaded = true
}
