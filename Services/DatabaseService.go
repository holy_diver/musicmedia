package Services

import (
	Settings "burak-avci/musicapp/Services/Settings"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func CreateDbContext() (*gorm.DB, error) {
	db, err := gorm.Open(postgres.Open(Settings.GetConnectionString()), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	return db, nil
}
