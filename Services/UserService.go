package Services

import (
	UserModels "burak-avci/musicapp/Models/User"
	JWTService "burak-avci/musicapp/Services/JWT"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserRepository gorm.DB

func AuthenticateUser(user UserModels.UserAuth) (bool, string, error) {
	db, err := CreateDbContext()
	if err != nil {
		return false, "", err
	}
	var dbUsr UserModels.User
	db.First(&dbUsr, "username = ?", user.Username)
	if err = bcrypt.CompareHashAndPassword([]byte(dbUsr.Password), []byte(user.Password)); err == nil {
		jwtServ := JWTService.NewJWTService()
		token, err := jwtServ.GenerateToken(user.Username)

		if err != nil {
			return false, "", err
		}

		return true, token, nil
	} else {
		return false, "", err
	}
}

func (db *UserRepository) userExists(username string) (bool, error) {
	gdb := (*gorm.DB)(db)
	count := int64(0)
	err := gdb.Table("users").Where("username = ?", username).Count(&count).Error
	if err != nil {
		return false, err
	}

	return count > 0, nil
}

func ValidateJWT(token string) (bool, error) {
	jwtServ := JWTService.NewJWTService()
	return jwtServ.ValidateToken(token)
}

func RegisterUser(user *UserModels.User) (bool, string, error) {

	jwtServ := JWTService.NewJWTService()

	db, err := CreateDbContext()

	if err != nil {
		return false, "", err
	}

	repo := (*UserRepository)(db)

	exists, err := repo.userExists(user.Username)

	if err != nil || exists {
		return false, "", err
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.MinCost)

	if err != nil {
		return false, "", err
	}

	user.Password = string(hash)

	result := db.Create(&user)

	if result.Error != nil {
		return false, "", err
	}

	token, err := jwtServ.GenerateToken(user.Username)
	if err != nil {
		return false, "", err
	}
	return true, token, nil

}
