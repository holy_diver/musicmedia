package main

import (
	UserModels "burak-avci/musicapp/Models/User"
	"burak-avci/musicapp/Services"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.POST("/Auth", AuthUser)
	router.POST("/Register", Register)

	userRoutes := router.Group("/social", ValidateJWT)
	{
		userRoutes.GET("/test", Test)
	}

	router.Run("localhost:8080")
}

func Test(c *gin.Context) {
	c.String(http.StatusOK, "%s", "test")
}

func ValidateJWT(c *gin.Context) {
	const BEARER_SCHEMA = "Bearer "
	authHeader := c.GetHeader("Authorization")
	tokenString := authHeader[len(BEARER_SCHEMA):]

	ok, err := Services.ValidateJWT(tokenString)

	if err != nil || !ok {
		c.AbortWithStatus(http.StatusBadRequest)
	}
}

func AuthUser(c *gin.Context) {
	var user UserModels.UserAuth

	if err := c.BindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, nil)
		return
	}

	res, token, err := Services.AuthenticateUser(user)

	if err != nil || !res {
		c.JSON(http.StatusUnauthorized, UserModels.AuthResult{Success: false, JWTToken: ""})
		return
	}

	c.JSON(http.StatusOK, UserModels.AuthResult{Success: true, JWTToken: token})
	return
}

func Register(c *gin.Context) {
	var user UserModels.User
	if err := c.BindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, nil)
		return
	}

	result, token, err := Services.RegisterUser(&user)

	if err != nil {
		c.JSON(http.StatusBadRequest, nil)
		return
	}

	if !result {
		c.JSON(http.StatusBadRequest, nil)
		return
	}

	c.JSON(http.StatusOK, UserModels.AuthResult{Success: true, JWTToken: token})
	return
}
